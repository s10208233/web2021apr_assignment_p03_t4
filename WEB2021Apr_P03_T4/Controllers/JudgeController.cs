﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using WEB2021Apr_P03_T4.Models;
using WEB2021Apr_P03_T4.DAL;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;


namespace WEB2021Apr_P03_T4.Controllers
{
    public class JudgeController : Controller
    {
        private JudgeDAL JudgeDAL = new JudgeDAL();
        private CriteriaDAL CriteriaDAL = new CriteriaDAL();



        public IActionResult CreateJudge()
        {
            return View();
        }

        public IActionResult CreateCriteria(int CompetitionID)
        {
            ViewData["compID"] = CompetitionID;
            ViewData["existingList"] = JudgeDAL.getCriteriaDetailsListByCompID(CompetitionID);
            return View();
        }
        public ActionResult ViewYourCompetitions()
        {
            var judgeid = HttpContext.Session.GetInt32("JudgeID");
            List<Competition> competitionList = JudgeDAL.GetYourCompetition((int)judgeid);
            return View(competitionList);
        }
        public ActionResult ViewYourCompetitors(int CompetitionID)
        {
            int CompetID = CompetitionID;
            List<PublicViewModel> competitionList = JudgeDAL.GetYourCompetitors(CompetID);
            ViewData["CompetID"] = CompetID;
            return View(competitionList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateJudge(Judge judge)
        {
            if (ModelState.IsValid)
            {
                JudgeDAL.Add(judge);
                return RedirectToAction("Login", "Home");
            }
            else
            {
                return View(judge);
            }
        }

        public ActionResult AddCritirion(Criteria crit) {
            CriteriaDAL.Add(crit);
            return RedirectToAction("CreateCriteria", "Judge", new { CompetitionID = crit.CompetitionID });
        }


        public ActionResult Delete(int compID, int critirionID) {
            CriteriaDAL.DeleteCritirion(compID, critirionID);
            return RedirectToAction("CreateCriteria", "Judge",  new { CompetitionID = compID });
        }

        public IActionResult EditRanking(int CompetitorID, int CompetitionID)
        {
            int CompetitionIDPrev = CompetitionID;
            int CompetID = CompetitorID;
            ViewData["CompetID"] = CompetID;
            CompetitionSubmission retrievedSubmission = JudgeDAL.GetCompetitionSubmission(CompetitionID, CompetitorID);
            ViewData["SubmissionAppeal"] = retrievedSubmission.Appeal;
            ViewData["VoteCountSubmission"] = retrievedSubmission.VoteCount;
            ViewData["Ranking"] = retrievedSubmission.Ranking;
            ViewData["totalMarks"] = JudgeDAL.getTotalMark(CompetitorID, CompetitionID);
            return View(retrievedSubmission);
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult EditRanking(IFormCollection formData)
        //{
        //    if (formData["rankingInput"] != "")
        //    {

        //    }
        //}


    }
}
