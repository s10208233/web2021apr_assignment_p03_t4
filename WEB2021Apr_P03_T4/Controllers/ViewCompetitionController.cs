﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.Models;
using WEB2021Apr_P03_T4.DAL;
using System.IO;


namespace WEB2021Apr_P03_T4.Controllers
{
    public class ViewCompetitionController : Controller
    {
        // GET: ViewCompetition
        private CompetitionDAL competitionContext = new CompetitionDAL();

        public ActionResult Index()
        {
            List<Competition> competitionList = competitionContext.GetCompetition();
            return View(competitionList);
        }

        public ActionResult ViewCompetitors(int competitionID)
        {
            List<PublicViewModel> submissionList = competitionContext.GetSubmissions();
            int CompetID = competitionID;
            ViewData["CompetID"] = CompetID;
            TempData["CompetID"] = CompetID;
            return View(submissionList);
        }
        public ActionResult ViewTopCompetitors(int competitionID)
        {
            List<PublicViewModel> submissionList = competitionContext.GetTopRanking();
            int CompetID = competitionID;
            ViewData["CompetID"] = CompetID;
            TempData["CompetID"] = CompetID;
            return View(submissionList);
        }
        public ActionResult ViewComments(int competitionID)
        {
            List<Comment> commentsList = competitionContext.GetComments();
            int CompetID = competitionID;
            ViewData["CompetID"] = CompetID;
            TempData["CompetID"] = CompetID;
            return View(commentsList);
        }
        public ActionResult CreateComment(int competitionID)
        {
            int CompetID = competitionID;
            ViewData["CompetID"] = CompetID;
            TempData["CompetID"] = CompetID;
            return View();
        }
        public ActionResult IncreaseVoteCount(int competitionID,int competitiorID, int voteCount)
        {
            competitionContext.UpdateVoteCount(competitionID,competitiorID,voteCount);
            return RedirectToAction("ViewCompetitors",new { CompetitionID = competitionID });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateComment(Comment comment)
        {
            if (ModelState.IsValid)
            {
                competitionContext.AddComment(comment);
                return RedirectToAction("ViewComments", "ViewCompetition" ,new {CompetitionID = comment.CompetitionID});
            }
            else
            {
                return View(comment);
            }
        }
        

    }
    
}
