﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.Models;
using WEB2021Apr_P03_T4.DAL;
using WEB2021Apr_P03_T4.Models;

namespace WEB2021Apr_P03_T4.Controllers
{
    public class AreaInterestController : Controller
    {
        private AreaOfInterestDAL aoiDal = new AreaOfInterestDAL();
        public IActionResult AreaInterestDashboard()
        {
            // Stop accessing the action if not logged in or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<AreaInterestViewModel> AreaInterestVMList = aoiDal.GetAreaInterestVMList();
            return View(AreaInterestVMList);
        }

        // GET: AreaInterest/Details/5
        public ActionResult ViewCompetition(int id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AreaInterestCreate(AreaInterest aoi)
        {
            try
            {
                aoiDal.Add(aoi);
                return RedirectToAction("AreaInterestDashboard", "Administrator");
            }
            catch
            {
                return RedirectToAction("AreaInterestDashboard", "Administrator");
            }
        }

        // GET: AreaInterest/Delete/5
        public ActionResult Delete(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            aoiDal.Delete(id.Value);
            return RedirectToAction("AreaInterestDashboard", "Administrator");

        }

        public ActionResult AddCompetitionForSelectedAOI(int? id) {
            return RedirectToAction();
        }


    }
}
