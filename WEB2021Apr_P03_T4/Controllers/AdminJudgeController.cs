﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.Models;
using WEB2021Apr_P03_T4.DAL;

namespace WEB2021Apr_P03_T4.Controllers
{
    public class AdminJudgeController : Controller
    {
        private AdminCompJudgeDal compjudgeDAL = new AdminCompJudgeDal();

        public ActionResult Remove(int compID, int judgeID)
        {

            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            if (compID == null || judgeID == null )
            {
                return RedirectToAction("Index", "Home");
            }

            compjudgeDAL.Delete(compID, judgeID);
            return RedirectToAction("CompJudgeDashboard", "Administrator");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Assign(CompetitionJudge compJudge)
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            compjudgeDAL.Add(compJudge);
            return RedirectToAction("CompJudgeDashboard", "Administrator");
        }

        public ActionResult AssignDouble(CompetitionDoubleJudge cdj)
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (cdj.JudgeID_first == cdj.JudgeID_second) {
                   TempData["doubleJudgeError"] = "Please Enter Two Unique Judges!";
                return RedirectToAction("CompJudgeAssignDouble", "Administrator");
            }
            compjudgeDAL.AddDouble(cdj);
            return RedirectToAction("CompJudgeDashboard", "Administrator");
        }
    }
}
