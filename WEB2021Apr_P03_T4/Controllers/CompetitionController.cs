﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.DAL;
using WEB2021Apr_P03_T4.Models;

namespace WEB2021Apr_P03_T4.Controllers
{
    public class CompetitionController : Controller
    {
        private CompetitionDAL CompetitionDal = new CompetitionDAL();
        private AreaOfInterestDAL aoiDal = new AreaOfInterestDAL();
        private AdminCompetitionDAL adminCompDAL = new AdminCompetitionDAL();
        // GET: Competition
        public ActionResult Index()
        {
            return View();
        }

        // GET: Competition/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Competition/Create
        public ActionResult Create()
        {
            return View();
        }

        //function to get all competitions
        public List<Competition> getCompetitions()
        {
            List<Competition> competitionList = CompetitionDal.GetCompetition();
            return competitionList;
        }

        //function to get all submissions from competitors
        public List<CompetitionSubmission> getSubmissions()
        {
            List<CompetitionSubmission> submissionList = CompetitionDal.GetSubmission();
            return submissionList;
        }

        //creating a competitor profile
        public ActionResult CreateCompetitor(Competitor competitor)
        {
            if (ModelState.IsValid)
            {
                //Add competitor record to database
                competitor.CompetitorID = CompetitionDal.Add(competitor);
                //Redirect user to Login view
                return RedirectToAction("Login", "Home");
            }
            else
            {
                //Input validation fails, return to the Create view
                //to display error message
                return View("CreateCompetitor");
            }
        }

        //Show the competitons available to join
        public ActionResult JoinCompetition(CompetitionSubmission join, DateTime startDate)
        {
            //check if user is logged in as a competitor if not redirect to index
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }

            return View(getCompetitions());
        }

        //action to add competition into sql table for competitor when they press join button
        [HttpPost]  
        public ActionResult Join(CompetitionSubmission join, string CompetitionID, DateTime startDate)
        {
            List<CompetitionSubmission> submissionList = getSubmissions();
            bool check = false;
            for (int i = 0; i < submissionList.Count; i++)
            {
                //Check if user already joined the competition
                if (submissionList[i].CompetitorID == Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")) && submissionList[i].CompetitionID == Convert.ToInt32(CompetitionID))
                {
                    TempData["Message"] = "You have already joined this competition.";
                    check = false;
                    break;
                }
                else { check = true; }
            }
                
            if (check == true)
            {
                //Check if its too late to join the competition
                if ((startDate - DateTime.Now).TotalDays > 3)
                {
                    //inserting into database
                    join.CompetitorID = Convert.ToInt32(HttpContext.Session.GetString("CompetitorID"));
                    join.CompetitionID = Convert.ToInt32(CompetitionID);
                    CompetitionDal.JoinCompetition(join);
                    TempData["MessageSuccess"] = "You have successfully joined the competition";
                    
                }
                else
                {
                    TempData["Message"] = "It is too late to join this competition";
                }
            }   
            
                
             
            return RedirectToAction("JoinCompetition");
        }
        //Retutns a view for the competitions the competitor has chose to join
        public ActionResult ViewCompetitorCompetition()
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<Competition> competitionList = getCompetitions();
            List<CompetitionSubmission> submissionList = getSubmissions();
            List<Competition> joinedList = new List<Competition>();
            for (int i = 0; i < submissionList.Count; i++)
            {
                if (submissionList[i].CompetitorID == Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")))
                {
                    for (int j = 0; j < competitionList.Count; j++)
                    {
                        if (submissionList[i].CompetitionID == competitionList[j].CompetitionID)
                        {
                            joinedList.Add(competitionList[j]);
                        }
                    }
                }

            }
            ViewBag.joinedList = joinedList;

            return View();
        }

        //Action method to allow user to upload their work into server
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> UploadPdf(CompetitionSubmission competitionSubmission,int id,DateTime startDate,DateTime endDate)
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }

            if (DateTime.Now < startDate)
            {
                TempData["UploadLateEarly"] = "It is too early to upload your work";
                return RedirectToAction("ViewCompetitorCompetition", "Competition");
            }
            else if (DateTime.Now > endDate)
            {
                TempData["UploadLateEarly"] = "It is too late to upload your work";
                return RedirectToAction("ViewCompetitorCompetition", "Competition");
            }
            else
            {
                if (competitionSubmission.FileSubmitted != null && competitionSubmission.FileSubmitted.Length > 0)
                {
                    try
                    {
                        // Find the filename extension of the file to be uploaded.
                        string fileExt = Path.GetExtension(
                        competitionSubmission.FileSubmitted.FileName);
                        // Rename the uploaded file with the competitionID and competitorID.
                        string uploadedFile ="File_"+ id + "_" + HttpContext.Session.GetString("CompetitorID") + fileExt;
                        // Get the complete path to the PDF folder where all the files will be stored
                        string savePath = Path.Combine(
                         Directory.GetCurrentDirectory(),
                         "wwwroot\\PDF", uploadedFile);
                        // Upload the file to server
                        using (var fileSteam = new FileStream(
                         savePath, FileMode.Create))
                        {
                            //Store the name into database
                            CompetitionDal.UploadPDF(uploadedFile, id, Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")));
                            //upload to file
                            await competitionSubmission.FileSubmitted.CopyToAsync(fileSteam);
                            TempData["UploadSuccess"] = "File uploaded successfully.";
                            return RedirectToAction("ViewCompetitorCompetition", "Competition");
                        }
                        
                    }
                    catch (IOException)
                    {
                        //File IO error, could be due to access rights denied
                        TempData["UploadFail"] = "File uploading fail!";
                        return RedirectToAction("ViewCompetitorCompetition", "Competition");

                    }
                }
            }



            return View(competitionSubmission);
        }
        //Returns to view to all the competiton that competitor has joined to allow them to appeal
        public ActionResult ViewAndAppeal()
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }
            //get all the require data from DAL
            List<Competition> competitionList = getCompetitions();
            List<CompetitionSubmission> submissionList = getSubmissions();
            List<Competition> joinedList = new List<Competition>();
            List<CompetitionScore> scoreList = new List<CompetitionScore>();
            List<Criteria> criteriaList = CompetitionDal.GetCriteria();
            List<CompetitionScore> competitionScoreList = CompetitionDal.GetCompetitionScore();

            //Looping thru the list to get the competitions joined
            for (int i = 0; i < submissionList.Count; i++)
            {
                if (submissionList[i].CompetitorID == Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")))
                {
                    for (int j = 0; j < competitionList.Count; j++)
                    {
                        if (submissionList[i].CompetitionID == competitionList[j].CompetitionID)
                        {
                            joinedList.Add(competitionList[j]);
                        }
                    }
                }

            }
            //Loop thru to find the data that relates to the comepetitor using competitorID
            for (int n = 0; n < competitionScoreList.Count; n++)
            {
                if (competitionScoreList[n].CompetitorID == Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")))
                {
                    scoreList.Add(competitionScoreList[n]);
                }
            }
            //Data needed to show on the View
            ViewBag.competitionScoreList = scoreList;
            ViewBag.criteriaList = criteriaList;
            ViewBag.joinedList = joinedList;
            return View();
        }
        //Submit the appeal basic textbox and submit button
        public ActionResult SubmitAppeal(CompetitionSubmission competitionSubmission, string value, int id,DateTime ResultReleasedDate)
        {
            
            
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Competitor"))
            {
                return RedirectToAction("Index", "Home");
            }

            List<CompetitionSubmission> submissionList = getSubmissions();
            //check if its too late
            if (DateTime.Now > ResultReleasedDate)
            {
                for (int i = 0; i < submissionList.Count; i++)
                {
                    if (submissionList[i].CompetitionID == id && submissionList[i].CompetitorID == Convert.ToInt32(HttpContext.Session.GetString("CompetitorID")))
                    {
                        //check if competitor has already appealed or not
                        if (submissionList[i].Appeal == null || submissionList[i].Appeal == "")
                        {
                            if (value != null)
                            {
                                TempData["AppealSuccess"] = "You have successfully appealed";
                                
                            }
                            if (CompetitionDal.SubmitAppeal(value, id, Convert.ToInt32(HttpContext.Session.GetString("CompetitorID"))) == true)
                            {
                                return RedirectToAction("ViewAndAppeal", "Competition");
                            }
                            


                        }
                        else
                        {
                            TempData["AppealFail"] = "You already appealed for this competition";
                            return RedirectToAction("ViewAndAppeal", "Competition");
                        }
                    }
                }
                
            }
            else
            {
                TempData["AppealFail"] = "It is too late to appeal";
                return RedirectToAction("ViewAndAppeal", "Competition");
            }
            ViewBag.CompetitionID = id;
            
            return View();
        }


        // POST: Competition/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Competition/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Competition/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, IFormCollection collection)
        {
            try
            {
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitCreateCompetition(Competition comp)
        {
            try
            {
                if (comp.StartDate > comp.EndDate) {
                    TempData["EndDateWrong"] = "The competition starting date has to be earlier than the ending date!";
                    return RedirectToAction("CompetitionCreate", "Administrator");
                }
                if (comp.EndDate > comp.ResultReleasedDate)
                {
                    TempData["ResRelWrong"] = "The competition ending date has to be earlier than the result release date!";
                    return RedirectToAction("CompetitionCreate", "Administrator");
                }
                adminCompDAL.Add(comp);
                //  REDIRECT TO VIEW COMPETITION
                return RedirectToAction("CompetitionDashboard", "Administrator");
            }
            catch
            {
                //  REDIRECT TO VIEW COMPETITION
                return RedirectToAction("CompetitionDashboard", "Administrator");
            }
        }

        public ActionResult Delete(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) || (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }

            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            adminCompDAL.Delete(id.Value);
            return RedirectToAction("CompetitionDashboard", "Administrator");

        }

    }
}
