﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.Models;
using WEB2021Apr_P03_T4.DAL;

namespace WEB2021Apr_P03_T4.Controllers
{
    //  ̿̿ ̿̿ ̿̿ ̿'̿'\̵͇̿̿\з= ( ▀ ͜͞ʖ▀) =ε/̵͇̿̿/’̿’̿ ̿ ̿̿ ̿̿ ̿̿     hasta la vista fuck asp

    public class AdministratorController : Controller
    {
        private AreaOfInterestDAL aoiDal = new AreaOfInterestDAL();
        private CompetitionDAL compDal = new CompetitionDAL();
        private AdminCompetitionDAL adminCompDal = new AdminCompetitionDAL();
        private AdminCompJudgeDal judgeAdminDAL = new AdminCompJudgeDal();

        public IActionResult Index()
        {
            return View();
        }


        //  AREAINTEREST REDIRECTION
        public IActionResult AreaInterestDashboard()
        {
            // Stop accessing the action if not logged in or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<AreaInterestViewModel> AreaInterestVMList = aoiDal.GetAreaInterestVMList();
            return View(AreaInterestVMList);
        }
        public ActionResult AreaInterestCreate()
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        public ActionResult AreaInterestDelete(int id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null) {
                return RedirectToAction("AreaInterestDashboard", "Administrator");
            }
            Models.AreaInterest aoi = aoiDal.GetDetails(id);
            if (aoi == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("AreaInterestDashboard", "Administrator");
            }
            return View(aoi);
        }


        //  COMPETITION REDIRECTION
        public IActionResult CompetitionDashboard()
        {
            // Stop accessing the action if not logged in or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<Competition> competitionList = compDal.GetCompetition();
            return View(competitionList);
        }

        public ActionResult CompetitionCreate(Competition competition) {
            ViewData["AreaInterestVM"] = aoiDal.GetAreaInterestVMList();
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View(competition);
        }

        public ActionResult CompetitionDelete(int? id)
        {
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            if (id == null)
            {
                return RedirectToAction("CompetitionDashboard", "Administrator");
            }
            Competition comp = adminCompDal.GetDetails((int)id);
            if (comp == null)
            {
                //Return to listing page, not allowed to edit
                return RedirectToAction("CompetitionDashboard", "Administrator");
            }
            return View(comp);
        }
        public IActionResult CompJudgeDashboard()
        {
            // Stop accessing the action if not logged in or account not in the "Staff" role
            if ((HttpContext.Session.GetString("Role") == null) ||
            (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            List<CompetitionJudgeViewModel> compjudgeListVM = judgeAdminDAL.GetCompetitionJudgeViewModel();
            return View(compjudgeListVM);
        }

        public ActionResult CompJudgeAssign(int? compID)
        {
            ViewData["judgelist"] = judgeAdminDAL.GetJudgeList();
            //ViewData["complist"] = judgeAdminDAL.GetCompetitionJudge_Alternating_List(2);
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            ViewData["CompID"] = compID;
            return View();
        }

        public ActionResult CompJudgeAssignDouble()
        {
            ViewData["judgelist"] = judgeAdminDAL.GetJudgeList();
            ViewData["complist"] = judgeAdminDAL.GetCompetitionJudge_Alternating_List(1);
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult CompJudgeDelete()
        {
            ViewData["judgelist"] = judgeAdminDAL.GetJudgeList();
            ViewData["complist"] = judgeAdminDAL.getCompetitionList();
            if ((HttpContext.Session.GetString("Role") == null) ||
           (HttpContext.Session.GetString("Role") != "Admin"))
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

    }
}
