﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.Models;
using WEB2021Apr_P03_T4.DAL;

namespace WEB2021Apr_P03_T4.Controllers
{
    public class HomeController : Controller
    {
        private UsersDAL userDal = new UsersDAL();

        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About() {
            return View();
        }
        public IActionResult Login() {
            return View();
        }

        [HttpPost]
        public ActionResult UserLogin(IFormCollection formData)
        {
            // Get Inputs
            string loginEmail = formData["inputEmail"].ToString();
            string loginPassword = formData["inputPassword"].ToString();
            


            // Get judgeList & competitorList
            List<Judge> judgeList = userDal.GetJudgeList();
            List<Competitor> competitorList = userDal.GetCompetitorList();


            // IF ADMIN IS ADMIN
            if (loginEmail == "admin1@lcu.edu.sg" && loginPassword == "p@55Admin")
            {
                HttpContext.Session.SetString("LoginEmail", loginEmail);
                HttpContext.Session.SetString("Role", "Admin");

                return RedirectToAction("AreaInterestDashboard", "Administrator");
            }

            // IF JUDGE
            for (int i = 0; i < judgeList.Count(); i++) {
                if (loginEmail == judgeList[i].EmailAddr && loginPassword == judgeList[i].Password)
                {
                    HttpContext.Session.SetString("LoginEmail", loginEmail);
                    HttpContext.Session.SetString("Role", "Judge");
                    HttpContext.Session.SetInt32("JudgeID", judgeList[i].JudgeID);

                    // Redirect user to the "StaffMain" view through an action
                    return RedirectToAction("Index", "Home");
                }
            }

            // IF COMPETITOR
            for (int i = 0; i < competitorList.Count(); i++)
            {
                if (loginEmail == competitorList[i].EmailAddr && loginPassword == competitorList[i].Password)
                {
                    HttpContext.Session.SetString("LoginEmail", loginEmail);
                    HttpContext.Session.SetString("Role", "Competitor");
                    HttpContext.Session.SetString("CompetitorID", Convert.ToString(competitorList[i].CompetitorID));

                    // Redirect user to the "JoinCompetition" view through an action
                    return RedirectToAction("JoinCompetition", "Competition");
                }
            }


            // LOGIN FAIL
            // Store an error message in TempData for display at the index view
            TempData["Message"] = "Invalid Login Credentials!";

            // Redirect user back to the index view through an action 
            return RedirectToAction("Login");
        }

        public IActionResult Logout() {
            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
