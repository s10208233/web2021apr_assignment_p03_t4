﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P03_T4.Models
{
    public class PublicViewModel
    {
        public int CompetitionID { get; set; }
        public int CompetitorID { get; set; }
        public string CompetitionName { get; set; }
        public string CompetitorName { get; set; }
        public string FileSubmitted { get; set; }
        public DateTime? DateTimeFileUpload { get; set; }
        public string Appeal { get; set; }
        public int VoteCount { get; set; }
        public int? Ranking { get; set; }
    }
}
