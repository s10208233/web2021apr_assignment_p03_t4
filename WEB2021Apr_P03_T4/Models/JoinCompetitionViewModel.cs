﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P03_T4.Models
{
    public class JoinCompetitionViewModel
    {
        public List<Competition> competitionVM { get; set; }

        [Display(Name = "Compeitition ID")]
        public int CompetitionID { get; set; }
        [Display(Name = "Competitor ID")]
        public int CompetitorID { get; set; }


        public CompetitionSubmission competitionSubmissionVM { get; set; }


        public JoinCompetitionViewModel()
        {
            competitionVM = new List<Competition>();
            

        }
    }
}
