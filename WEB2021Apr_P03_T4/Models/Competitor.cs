﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P03_T4.Models
{
    public class Competitor
    {
        [Required(ErrorMessage = "Competitor ID is required")]
        [Display(Name = "Competitor ID")]
        public int CompetitorID { get; set; }



        [Required(ErrorMessage = "Competitor Name is required")]
        [Display(Name = "Competitor Name")]
        [StringLength(50, ErrorMessage = "Competitor Name cannot be more than 50 characters.")]
        public string CompetitorName { get; set; }



        [StringLength(5, ErrorMessage = "Salutation cannot be more than 5 characters.")]
        public string Salutation { get; set; }



        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Invalid Email")]
        [ValidateEmailExist]
        [StringLength(50, ErrorMessage = "Competitor Name cannot be more than 50 characters.")]
        [Display(Name = "Email Address")]
        public string EmailAddr { get; set; }
       



        [Required(ErrorMessage = "Password is required")]
        [StringLength(255, ErrorMessage = "Password cannot be more than 255 characters.")]
        public string Password { get; set; }
    }
}
