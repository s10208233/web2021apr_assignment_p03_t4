﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using WEB2021Apr_P03_T4.DAL;

namespace WEB2021Apr_P03_T4.Models
{
    public class ValidateEmailExist : ValidationAttribute
    {
        private CompetitionDAL CompetitionDal = new CompetitionDAL();
        protected override ValidationResult IsValid(
        object value, ValidationContext validationContext)
        {
            // Get the email value to validate
            string email = Convert.ToString(value);
            // Casting the validation context to the "Staff" model class
            Competitor competitor = (Competitor)validationContext.ObjectInstance;
            // Get the Staff Id from the staff instance
            int competitorID = competitor.CompetitorID;
            if (CompetitionDal.IsEmailExist(email, competitorID))
                // validation failed
                return new ValidationResult
                ("Email address already exists!");
            else
                // validation passed
                return ValidationResult.Success;
        }

    }
}
