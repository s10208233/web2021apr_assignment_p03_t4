﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace WEB2021Apr_P03_T4.Models
{
    public class CompetitionSubmission
    {
        [Required(ErrorMessage = "Competiton ID is required")]
        [Display(Name = "Compeitition ID")]
        public int CompetitionID { get; set; }

        [Required(ErrorMessage = "Competitor ID is required")]
        [Display(Name = "Competitor ID")]
        public int CompetitorID { get; set; }

        [StringLength(250)]
        public IFormFile FileSubmitted { get; set; }

        [Display(Name = "Date and Time of file upload")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy H:mm:ss}")]
        public DateTime? DateTimeFileUpload { get; set; }

        [StringLength(255, ErrorMessage = "Appeal cannot exceed 255 letters.")]
        public string Appeal { get; set; }

        [Display(Name ="Vote count")]
        [Required(ErrorMessage ="Vote count is missing")]
        public int VoteCount { get; set; }


        public int? Ranking { get; set; }
    }
}
