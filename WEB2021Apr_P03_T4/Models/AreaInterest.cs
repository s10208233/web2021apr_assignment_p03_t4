﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P03_T4.Models
{
    public class AreaInterest
    {
        [Required(ErrorMessage = "Area of Interest ID is required")]
        [Display(Name = "Area of Interest ID")]
        public int AreaInterestID { get; set; }

        [Required(ErrorMessage = "Area of Interest Name is required")]
        [Display(Name = "Area of Interest Name")]
        [StringLength(50, ErrorMessage = "Area of Interest Name cannot be more than 50 characters.")]
        public string Name { get; set; }
    }
}
