﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P03_T4.Models
{
    public class Comment
    {
        [Required(ErrorMessage = "Comment ID is missing")]
        public int CommentID { get; set; }
            
        [Required(ErrorMessage = "Competitor ID is required")]
        [Display(Name = "Compeitition ID")]
        public int CompetitionID { get; set; }

        [StringLength(255, ErrorMessage = "Description of comment cannot be more than 255 characters.")]
        public string Description { get; set; }

        [Display(Name = "Date and Time of comment")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DateTimePosted { get; set; }
    }
}
