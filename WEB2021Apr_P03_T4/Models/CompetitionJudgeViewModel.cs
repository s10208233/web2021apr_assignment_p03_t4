﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P03_T4.Models
{
    public class CompetitionJudgeViewModel
    {
        [Required(ErrorMessage = "Judge ID is required")]
        [Display(Name = "Judge ID")]
        public int JudgeID { get; set; }

        [Required(ErrorMessage = "Judge Name is required")]
        [Display(Name = "Judge Name")]
        [StringLength(50, ErrorMessage = "Judge Name cannot be longer than 50 characters.")]
        public string JudgeName { get; set; }

        [Required(ErrorMessage = "Area of Interest Name is required")]
        [Display(Name = "Area of Interest Name")]
        [StringLength(50, ErrorMessage = "Area of Interest Name cannot be more than 50 characters.")]
        public string AreaInterestName { get; set; }

        [Required(ErrorMessage = "Competition Name is required")]
        [Display(Name = "Compeitition Name")]
        [StringLength(50, ErrorMessage = "Area of Interest Name cannot be more than 50 characters.")]
        public string CompetitionName { get; set; }

        [Required(ErrorMessage = "Competitor ID is required")]
        [Display(Name = "Compeitition ID")]
        public int CompetitionID { get; set; }
    }
}
