﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WEB2021Apr_P03_T4.Models
{
    public class CompetitionJudge
    {
        [Required(ErrorMessage ="Competition ID is reqruired")]
        [Display(Name ="Competition ID")]
        public int CompetitionID { get; set; }

        [Required(ErrorMessage = "Judge ID is required")]
        [Display(Name = "Judge ID")]
        public int JudgeID { get; set; }
    }
}
