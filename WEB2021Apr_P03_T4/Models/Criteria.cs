﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace WEB2021Apr_P03_T4.Models
{
    public class Criteria
    {
        [Required(ErrorMessage ="Criteria ID required")]
        [Display(Name = "Criteria ID")]
        public int CriteriaID { get; set; }

        [Required(ErrorMessage = "Competitor ID is required")]
        [Display(Name = "Compeitition ID")]
        public int CompetitionID { get; set; }

        [Required(ErrorMessage = "Criteria name is required")]
        [Display (Name = "Criteria Name")]
        [StringLength(50, ErrorMessage = "Area of Interest Name cannot be more than 50 characters.")]
        public string CriteriaName { get; set; }

        [Required(ErrorMessage = "Weightage is required")]
        public int Weightage { get; set; }

        //public Criteria()
        //{
        //    Weightage = 1;
        //}
    }
}
