﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace WEB2021Apr_P03_T4.Models
{
    public class Judge
    {
        [Required(ErrorMessage = "Judge ID is required")]
        [Display(Name ="Judge ID")]
        public int JudgeID { get; set; }

        [Required(ErrorMessage ="Judge Name is required")]
        [Display(Name = "Judge Name")]
        [StringLength(50, ErrorMessage ="Judge Name cannot be longer than 50 characters.")]
        public string JudgeName { get; set; }

        [StringLength(5, ErrorMessage ="Salutation cannot be more than 5 characters")]
        public string Salutation { get; set; }

        [Display(Name ="Area of Interest ID")]
        public int AreaInterestID { get; set; }

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "Invalid Email")]
        [Display(Name = "Email Address")]
        [StringLength(50, ErrorMessage = "Competitor Name cannot be more than 50 characters.")]
        [JudgeValidateEmailExists]
        public string EmailAddr { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(255, ErrorMessage = "Password cannot be more than 255 characters.")]
        public string Password { get; set; }
    }
}
