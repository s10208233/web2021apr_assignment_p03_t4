﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P03_T4.Models
{
    public class CompetitionScore
    {
        [Required(ErrorMessage = "Criteria ID required")]
        [Display(Name = "Criteria ID")]
        public int CriteriaID { get; set; }

        [Required(ErrorMessage = "Competitor ID is required")]
        [Display(Name = "Competitor ID")]
        public int CompetitorID { get; set; }

        [Required(ErrorMessage = "Competitor ID is required")]
        [Display(Name = "Compeitition ID")]
        public int CompetitionID { get; set; }

        //Regex to be figured out
        [RegularExpression(@"")]
        [Required(ErrorMessage = "Score is required")]
        public int Score { get; set; }

        [Display(Name = "Date and Time of last edit")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy H:mm:ss}")]
        public DateTime? DateTimeLastEdit { get; set; }

        public CompetitionScore()
        {
            this.Score = 0;
        }
    }
}
