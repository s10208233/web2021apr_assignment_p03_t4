﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WEB2021Apr_P03_T4.Models
{
    public class Competition
    {
        [Required(ErrorMessage = "Competitor ID is required")]
        [Display(Name = "Compeitition ID")]
        public int CompetitionID { get; set; }

        [Required(ErrorMessage = "Area of Interest ID is required")]
        [Display(Name = "Area of Interest ID")]
        public int AreaInterestID { get; set; }

        [Required(ErrorMessage = "Competition Name is required")]
        [Display(Name = "Compeitition Name")]
        [StringLength(50, ErrorMessage = "Area of Interest Name cannot be more than 50 characters.")]
        public string CompetitionName { get; set; }

        [Display(Name = "Starting Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "Ending Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? EndDate { get; set; }

        [Display(Name = "Result Release Date")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? ResultReleasedDate { get; set; }
    }
}
