#pragma checksum "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "79ddf3393abeaa47593f6d5bdab6688dc8d80140"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Competition_ViewAndAppeal), @"mvc.1.0.view", @"/Views/Competition/ViewAndAppeal.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\_ViewImports.cshtml"
using WEB2021Apr_P03_T4;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\_ViewImports.cshtml"
using WEB2021Apr_P03_T4.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"79ddf3393abeaa47593f6d5bdab6688dc8d80140", @"/Views/Competition/ViewAndAppeal.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"3ec97bf4248364c8935027b68722a9a73b2baaf6", @"/Views/_ViewImports.cshtml")]
    public class Views_Competition_ViewAndAppeal : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<WEB2021Apr_P03_T4.Models.Competition>>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "SubmitAppeal", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("submit"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("Value", new global::Microsoft.AspNetCore.Html.HtmlString("Appeal"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("position:absolute;right:40px;bottom:35px;background-color:orange;border-radius:5px;"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.FormActionTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
  
    ViewData["Title"] = "ViewAndAppeal";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n\r\n\r\n\r\n<h1 style=\"text-align: center\">Competition Scores</h1>\r\n<span style=\"color:lawngreen;\" id=\"success\">");
#nullable restore
#line 11 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
                                       Write(TempData["AppealSuccess"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n<span style=\"color:red;\">");
#nullable restore
#line 12 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
                    Write(TempData["AppealFail"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</span>\r\n\r\n");
#nullable restore
#line 14 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
 if (ViewBag.joinedList.Count > 0)
{
    

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
     foreach (var item in ViewBag.joinedList)
    {
        

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
         using (Html.BeginForm())
        {

#line default
#line hidden
#nullable disable
            WriteLiteral(@"            <div class=""container"" style=""background-color: rgb(255 255 255 / 0.10);height:300px;width:82%;margin-top:30px;margin-bottom:30px;position:relative;"">
                <div style=""position:absolute;left:35px;top:35px;height:230px;width:230px;background-color:white;""></div>
                <div style=""position:absolute;left:300px;top:50px;"">
                    ");
#nullable restore
#line 23 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
               Write(item.Name.ToString());

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    <ul style=\"margin-top:30px;list-style:none;\">\r\n");
#nullable restore
#line 25 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
                         foreach (var criteria in ViewBag.criteriaList)
                        {
                            if (item.CompetitionID == criteria.CompetitionID)
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <li>");
#nullable restore
#line 29 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
                               Write(criteria.CriteriaName);

#line default
#line hidden
#nullable disable
            WriteLiteral("</li>\r\n");
#nullable restore
#line 30 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
                            }
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </ul>\r\n                    <ul style=\"position:absolute;list-style:none;right:-170px;top:52px;\">\r\n");
#nullable restore
#line 35 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
                         foreach (var score in ViewBag.competitionScoreList)
                        {
                            if (item.CompetitionID == score.CompetitionID)
                            {

#line default
#line hidden
#nullable disable
            WriteLiteral("                                <li>Score : ");
#nullable restore
#line 39 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
                                       Write(score.Score);

#line default
#line hidden
#nullable disable
            WriteLiteral("</li>\r\n");
#nullable restore
#line 40 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
                            }
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    </ul>\r\n                </div>\r\n\r\n\r\n                ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("input", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagOnly, "79ddf3393abeaa47593f6d5bdab6688dc8d801409314", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.FormActionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.FormActionTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 47 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
                                                   WriteLiteral(item.CompetitionID);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            BeginWriteTagHelperAttribute();
#nullable restore
#line 47 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
                                                                                                 WriteLiteral(item.ResultReleasedDate);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues["resultEndDate"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-resultEndDate", __Microsoft_AspNetCore_Mvc_TagHelpers_FormActionTagHelper.RouteValues["resultEndDate"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n            </div>\r\n");
#nullable restore
#line 56 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"

        }

#line default
#line hidden
#nullable disable
#nullable restore
#line 57 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
         

    }

#line default
#line hidden
#nullable disable
#nullable restore
#line 59 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
     
}
else
{

#line default
#line hidden
#nullable disable
            WriteLiteral("    <span style=\"color:red\">No record found!</span>\r\n");
#nullable restore
#line 64 "C:\Users\leste\Desktop\Competition Judging\WEB2021Apr_P03_T4\Views\Competition\ViewAndAppeal.cshtml"
}

#line default
#line hidden
#nullable disable
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<WEB2021Apr_P03_T4.Models.Competition>> Html { get; private set; }
    }
}
#pragma warning restore 1591
