﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.Models;


namespace WEB2021Apr_P03_T4.DAL
{
    public class AdminCompJudgeDal
    {
        IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public AdminCompJudgeDal()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CJP_DB_ConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public List<CompetitionJudgeViewModel> GetCompetitionJudgeViewModel()
        {

            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"
                              SELECT        CompetitionJudge.JudgeID,
                             (SELECT        JudgeName
                               FROM            Judge
                               WHERE        (JudgeID = CompetitionJudge.JudgeID)) AS Expr1, AreaInterest.Name AS AreaInterestName, CompetitionJudge.CompetitionID,
                             (SELECT        CompetitionName
                               FROM            Competition
                               WHERE        (CompetitionID = CompetitionJudge.CompetitionID)) AS Expr2
FROM            AreaInterest INNER JOIN
                         Judge AS Judge_1 ON AreaInterest.AreaInterestID = Judge_1.AreaInterestID INNER JOIN
                         CompetitionJudge ON Judge_1.JudgeID = CompetitionJudge.JudgeID LEFT OUTER JOIN
                         Competition AS Competition_1 ON AreaInterest.AreaInterestID = Competition_1.AreaInterestID AND CompetitionJudge.CompetitionID = Competition_1.CompetitionID
GROUP BY CompetitionJudge.JudgeID, Judge_1.JudgeName, AreaInterest.Name, CompetitionJudge.CompetitionID, Competition_1.CompetitionName
ORDER BY CompetitionJudge.JudgeID
                                ";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<CompetitionJudgeViewModel> compJudgeVMList = new List<CompetitionJudgeViewModel>();
            while (reader.Read())
            {
                int judgeID = 0;
                string judgeName = "-";
                string AreaInterestName = "-";
                string CompetitionName = "-";
                int CompetitionID = 0;

                //DateTime? sd = null;
                //DateTime? ed = null;
                //DateTime? rrd = null;

                if (!reader.IsDBNull(0))
                    judgeID = reader.GetInt32(0);

                if (!reader.IsDBNull(1))
                    judgeName = reader.GetString(1);

                if (!reader.IsDBNull(2))
                    AreaInterestName = reader.GetString(2);

                if (!reader.IsDBNull(3))
                    CompetitionID = reader.GetInt32(3);

                if (!reader.IsDBNull(4))
                    CompetitionName = reader.GetString(4);

                compJudgeVMList.Add(
                new CompetitionJudgeViewModel
                {
                    JudgeID = judgeID,
                    JudgeName = judgeName,
                    AreaInterestName = AreaInterestName,
                    CompetitionName = CompetitionName,
                    CompetitionID = CompetitionID
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return compJudgeVMList;
        }

        public List<CompetitionJudgeViewModel> GetCompetitionJudge_Alternating_List(int choice)
        {

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"
                             SELECT        Judge.JudgeID, Judge.JudgeName, Competition.CompetitionID, Competition.CompetitionName
FROM            Judge FULL OUTER JOIN
                         CompetitionJudge ON Judge.JudgeID = CompetitionJudge.JudgeID FULL OUTER JOIN
                         Competition ON CompetitionJudge.CompetitionID = Competition.CompetitionID
                                ";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<CompetitionJudgeViewModel> combinedList = new List<CompetitionJudgeViewModel>();
            while (reader.Read())
            {
                int judgeID = 0;
                string judgeName = "-";
                string CompetitionName = "-";
                int CompetitionID = 0;

                //  Not Using
                string AreaInterestName = "-";

                if (!reader.IsDBNull(0))
                    judgeID = reader.GetInt32(0);

                if (!reader.IsDBNull(1))
                    judgeName = reader.GetString(1);

                if (!reader.IsDBNull(2))
                    CompetitionID = reader.GetInt32(2);

                if (!reader.IsDBNull(3))
                    CompetitionName = reader.GetString(3);


                combinedList.Add(
                new CompetitionJudgeViewModel
                {
                    JudgeID = judgeID,
                    JudgeName = judgeName,
                    AreaInterestName = AreaInterestName,
                    CompetitionName = CompetitionName,
                    CompetitionID = CompetitionID
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();

            List<CompetitionJudgeViewModel> competitionUnassignedJudges = new List<CompetitionJudgeViewModel>();
            List<CompetitionJudgeViewModel> assignedJudges = new List<CompetitionJudgeViewModel>();

            //  Sort The Damn Weird List
            for (int i = 0; i < combinedList.Count; i++)
            {
                if (combinedList[i].JudgeName == "-" && combinedList[i].JudgeID == 0)
                {
                    competitionUnassignedJudges.Add(new CompetitionJudgeViewModel
                    {
                        JudgeID = combinedList[i].JudgeID,
                        JudgeName = combinedList[i].JudgeName,
                        AreaInterestName = combinedList[i].AreaInterestName,
                        CompetitionID = combinedList[i].CompetitionID,
                        CompetitionName = combinedList[i].CompetitionName
                    });
                }
                if (combinedList[i].JudgeName != "-" && combinedList[i].JudgeID != 0)
                {
                    assignedJudges.Add(new CompetitionJudgeViewModel
                    {
                        JudgeID = combinedList[i].JudgeID,
                        JudgeName = combinedList[i].JudgeName,
                        AreaInterestName = combinedList[i].AreaInterestName,
                        CompetitionID = combinedList[i].CompetitionID,
                        CompetitionName = combinedList[i].CompetitionName
                    });
                }
            }

            //  Clean judgesWithNoCompetitions List from "-"
            for (int i = 0; i < competitionUnassignedJudges.Count; i++)
            {
                if (competitionUnassignedJudges[i].CompetitionID == 0)
                {
                    competitionUnassignedJudges.RemoveAt(i);
                }
            }

            //  Clean assignedJudges List from "-"
            for (int i = 0; i < assignedJudges.Count; i++) {
                if (assignedJudges[i].JudgeID == 0) {
                    assignedJudges.RemoveAt(i);
                }
            }

            if (choice == 1) {
            //  Choice 1 returns List<CompetitionJudgeViewModel> of: Unassigned Judges
            return competitionUnassignedJudges;
            }
            if (choice == 2) {
                //  Choice 2 returns List<CompetitionJudgeViewModel> of: Judges have been assigned
                return assignedJudges;
            }

            return combinedList;
        }

        public List<CompetitionJudge_mini_ViewModel> CompetitionJudgeList_ForAddingJudgesOnly() {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"
                             SELECT        Judge.JudgeID, Judge.JudgeName, Competition.CompetitionID, Competition.CompetitionName
FROM            Judge FULL OUTER JOIN
                         CompetitionJudge ON Judge.JudgeID = CompetitionJudge.JudgeID FULL OUTER JOIN
                         Competition ON CompetitionJudge.CompetitionID = Competition.CompetitionID
                                ";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<CompetitionJudge_mini_ViewModel> the_damned_list = new List<CompetitionJudge_mini_ViewModel>();
            while (reader.Read())
            {
                int judgeID = 0;
                string judgeName = "-";
                string CompetitionName = "-";
                int CompetitionID = 0;

                if (!reader.IsDBNull(0))
                    judgeID = reader.GetInt32(0);

                if (!reader.IsDBNull(1))
                    judgeName = reader.GetString(1);

                if (!reader.IsDBNull(2))
                    CompetitionID = reader.GetInt32(2);

                if (!reader.IsDBNull(3))
                    CompetitionName = reader.GetString(3);


                the_damned_list.Add(
                new CompetitionJudge_mini_ViewModel
                {
                    JudgeID = judgeID,
                    JudgeName = judgeName,
                    CompetitionName = CompetitionName,
                    CompetitionID = CompetitionID
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return the_damned_list;
        }


        public int Add(CompetitionJudge comJudge)
        {
            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"INSERT INTO CompetitionJudge (CompetitionID, JudgeID)
                                VALUES(@compID, @judgeID)";

            cmd.Parameters.AddWithValue("@compID", comJudge.CompetitionID);
            cmd.Parameters.AddWithValue("@judgeID", comJudge.JudgeID);

            conn.Open();

            int rowAffected = cmd.ExecuteNonQuery();

            conn.Close();

            return rowAffected;
        }

        public void AddDouble(CompetitionDoubleJudge comDoubleJudge)
        {
            AdminCompJudgeDal lol = new AdminCompJudgeDal();
            lol.Add(new CompetitionJudge {CompetitionID = comDoubleJudge.CompetitionID, JudgeID = comDoubleJudge.JudgeID_first });
            lol.Add(new CompetitionJudge { CompetitionID = comDoubleJudge.CompetitionID, JudgeID = comDoubleJudge.JudgeID_second });
        }

        public CompetitionJudgeViewModel GetDetails(int compID, int judgeID)
        {

            CompetitionJudgeViewModel cjvm = new CompetitionJudgeViewModel();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"
SELECT        CompetitionJudge.JudgeID, Judge.JudgeName, AreaInterest.Name AS AreaInterestName, CompetitionJudge.CompetitionID, Competition.CompetitionName
FROM            AreaInterest LEFT OUTER JOIN
                         Competition ON AreaInterest.AreaInterestID = Competition.AreaInterestID LEFT OUTER JOIN
                         CompetitionJudge ON Competition.CompetitionID = CompetitionJudge.CompetitionID FULL OUTER JOIN
                         Judge ON AreaInterest.AreaInterestID = Judge.AreaInterestID AND CompetitionJudge.JudgeID = Judge.JudgeID
						 WHERE Competition.CompetitionID = @compID AND CompetitionJudge.JudgeID = @judgeID 
                         ";
            cmd.Parameters.AddWithValue("@compID", compID);
            cmd.Parameters.AddWithValue("@judgeID", judgeID);

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    string judgeName = "-";
                    string AreaInterestName = "-";
                    string CompetitionName = "-";
                    int CompetitionID = 0;

                    //DateTime? sd = null;
                    //DateTime? ed = null;
                    //DateTime? rrd = null;

                    if (!reader.IsDBNull(0))
                        judgeID = reader.GetInt32(0);

                    if (!reader.IsDBNull(1))
                        judgeName = reader.GetString(1);

                    if (!reader.IsDBNull(2))
                        AreaInterestName = reader.GetString(2);

                    if (!reader.IsDBNull(3))
                        CompetitionName = reader.GetString(3);

                    if (!reader.IsDBNull(4))
                        CompetitionID = reader.GetInt32(4);

                    cjvm.JudgeID = judgeID;
                    cjvm.JudgeName = judgeName;
                    cjvm.AreaInterestName = AreaInterestName;
                    cjvm.CompetitionName = CompetitionName;
                    cjvm.CompetitionID = CompetitionID;
                }
            }
            reader.Close();
            conn.Close();
            return cjvm;
        }

        public int Delete(int compID, int judgeID)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM CompetitionJudge WHERE CompetitionID = @compID AND JudgeID = @judgeID";

            cmd.Parameters.AddWithValue("@compID", compID);
            cmd.Parameters.AddWithValue("@judgeID", judgeID);

            conn.Open();
            int rowAffected = 0;
            rowAffected += cmd.ExecuteNonQuery();
            conn.Close();
            return rowAffected;
        }

        public List<Judge> GetJudgeList() {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"select * from Judge";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<Judge> judgeLiist = new List<Judge>();
            while (reader.Read())
            {
                int judgeID = 0;
                string judgeName = "-";
                string salutation = "-";
                int aoiID = 0;
                string email = "-";
                string pw = "-";

                if (!reader.IsDBNull(0))
                    judgeID = reader.GetInt32(0);

                if (!reader.IsDBNull(1))
                    judgeName = reader.GetString(1);

                if (!reader.IsDBNull(2))
                    salutation = reader.GetString(2);

                if (!reader.IsDBNull(3))
                    aoiID = reader.GetInt32(3);

                if (!reader.IsDBNull(4))
                    email = reader.GetString(4);

                if (!reader.IsDBNull(5))
                    pw = reader.GetString(5);

                judgeLiist.Add(
                new Judge
                {
                    JudgeID = judgeID,
                    JudgeName = judgeName,
                    Salutation = salutation,
                    AreaInterestID = aoiID,
                    EmailAddr = email,
                    Password = pw
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return judgeLiist;
        }

        public List<Competition> getCompetitionList() {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"select * from Competition";
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<Competition> compList = new List<Competition>();
            while (reader.Read())
            {

                DateTime? sd = null;
                DateTime? ed = null;
                DateTime? rrd = null;

                if (!reader.IsDBNull(3))
                    sd = reader.GetDateTime(3);

                if (!reader.IsDBNull(4))
                    ed = reader.GetDateTime(4);

                if (!reader.IsDBNull(5))
                    rrd = reader.GetDateTime(5);

                compList.Add(
                new Competition
                {
                    CompetitionID = reader.GetInt32(0),
                    AreaInterestID = reader.GetInt32(1),
                    CompetitionName = reader.GetString(2),
                    StartDate = sd,
                    EndDate = ed,
                    ResultReleasedDate = rrd
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return compList;
        }
    }
}
