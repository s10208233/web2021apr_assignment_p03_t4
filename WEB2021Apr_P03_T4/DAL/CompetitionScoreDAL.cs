﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using WEB2021Apr_P03_T4.Models;
using Microsoft.Extensions.Configuration;

namespace WEB2021Apr_P03_T4.DAL
{
    public class CompetitionScoreDAL
    {
        IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public CompetitionScoreDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CJP_DB_ConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        //Retrieving scores
        public List<List<int>> RetrieveScores(int competitorID, int competitionID)
        {
            //**TO DO: Default value of score is 0, should set under controller to 0 if input is null

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM CompetitionScore
             WHERE CriteriaID=@competitorID AND CompetitionID=@competitionID";

            cmd.Parameters.AddWithValue("@competitorID", competitorID);
            cmd.Parameters.AddWithValue("@competitionID", competitionID);

            List<List<int>> outerList = new List<List<int>>();

            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    List<int> nestedList = new List<int>();

                    //Adds all criteriaID and respectivee score to nested List
                    nestedList.Add(reader.GetInt32(0));
                    nestedList.Add(reader.GetInt32(3));

                    //Adds 
                    outerList.Add(nestedList);
                }
            }
            //If nothing is found, just return empty list
            //Set logic in Controller to read List, and if List empty, return error or TBD
            return outerList;
        }

        public bool UpdateScores(int criteriaID, int competitorID, int competitionID, int score)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE CompetitionScore SET Criteria=@criteriaid,
                              CompetitorID=@competitorid, BranchNo = @competitionid, Score=@score
                              WHERE CompetitorID = @competitorid AND CompetitionID = @competitionid AND CriteriaID = @criteriaid";

            cmd.Parameters.AddWithValue("@criteriaid", criteriaID);
            cmd.Parameters.AddWithValue("@competitionid", competitionID);
            cmd.Parameters.AddWithValue("@score", score);
            cmd.Parameters.AddWithValue("@competitorid", competitorID);

            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            return true;
        }
    }
}
