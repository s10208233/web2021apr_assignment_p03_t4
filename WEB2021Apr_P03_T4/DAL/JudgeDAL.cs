﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.Models;

namespace WEB2021Apr_P03_T4.DAL
{
    public class JudgeDAL
    {
        IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public JudgeDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CJP_DB_ConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String re  ad.
            conn = new SqlConnection(strConn);
        }

        //Adding a new Judge to the DB, done if IsEmailExist = False
        public bool Add(Judge judge)
        {
            bool isSuccessful = false;
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO Judge (JudgeName, Salutation, AreaInterestID, EmailAddr,
                                Password)
                                OUTPUT INSERTED.JudgeID
                                VALUES(@name, @salutation, @interestid, @email,
                                @password)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@name", judge.JudgeName);
            cmd.Parameters.AddWithValue("@salutation", judge.Salutation);
            cmd.Parameters.AddWithValue("@interestid", judge.AreaInterestID);
            cmd.Parameters.AddWithValue("@email", judge.EmailAddr);
            cmd.Parameters.AddWithValue("@password", judge.Password);

            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //JudgeID after executing the INSERT SQL statement
            judge.JudgeID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return isSuccessful;
        }

        //For when a new Judge creates a profile, checks if email is unique
        public bool IsEmailExist(string email, int judgeid)
        {
            bool emailFound = false;
            //Create a SqlCommand object and specify the SQL statement
            //to get a staff record with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT JudgeID FROM Judge
             WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetInt32(0) != judgeid)
                        //The email address is used by another staff
                        emailFound = true;
                }
            }
            else
            { //No record
                emailFound = false; // The email address given does not exist
            }
            reader.Close();
            conn.Close();

            return emailFound;
        }
        public List<Competition> GetYourCompetition(int judgeID)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT        Competition.*, CompetitionJudge.JudgeID
                                FROM            CompetitionJudge INNER JOIN
                                Competition ON CompetitionJudge.CompetitionID = Competition.CompetitionID where JudgeID = @judgeID";
            cmd.Parameters.AddWithValue("@judgeID", judgeID);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a competition list
            List<Competition> competitionList = new List<Competition>();
            while (reader.Read())
            {
                DateTime? sd = null;
                DateTime? ed = null;
                DateTime? rrd = null;

                if (!reader.IsDBNull(3))
                    sd = reader.GetDateTime(3);

                if (!reader.IsDBNull(4))
                    ed = reader.GetDateTime(4);

                if (!reader.IsDBNull(5))
                    rrd = reader.GetDateTime(5);

                competitionList.Add(
                new Competition
                {
                    CompetitionID = reader.GetInt32(0),
                    AreaInterestID = reader.GetInt32(1),
                    CompetitionName = reader.GetString(2),
                    StartDate = sd,
                    EndDate = ed,
                    ResultReleasedDate = rrd
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return competitionList;
        }
        public List<PublicViewModel> GetYourCompetitors(int CompetID)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"select cs.CompetitionID,cs.CompetitorID,Com.CompetitionName,c.CompetitorName,cs.FileSubmitted,cs.DateTimeFileUpload,cs.Appeal,
                                cs.VoteCount,cs.Ranking FROM CompetitionSubmission cs INNER JOIN Competitor c on  cs.CompetitorID= 
                                c.CompetitorID INNER JOIN Competition com ON cs.CompetitionID= com.CompetitionID where cs.CompetitorID=@CompetID";
            cmd.Parameters.AddWithValue("@CompetID", CompetID);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<PublicViewModel> submissionList = new List<PublicViewModel>();
            while (reader.Read())
            {
                string fileName = "No file";
                DateTime? dtfu = null;
                string appeal = "No appeal";
                int vc = 0;
                int rank = 0;

                if (!reader.IsDBNull(4))
                    fileName = reader.GetString(4);

                if (!reader.IsDBNull(5))
                    dtfu = reader.GetDateTime(5);

                if (!reader.IsDBNull(6))
                    appeal = reader.GetString(6);

                if (!reader.IsDBNull(7))
                    vc = reader.GetInt32(7);

                if (!reader.IsDBNull(8))
                    rank = reader.GetInt32(8);

                submissionList.Add(
                new PublicViewModel
                {
                    CompetitionID = reader.GetInt32(0),
                    CompetitorID = reader.GetInt32(1),
                    CompetitionName = reader.GetString(2),
                    CompetitorName = reader.GetString(3),
                    FileSubmitted = fileName,
                    DateTimeFileUpload = dtfu,
                    Appeal = appeal,
                    VoteCount = vc,
                    Ranking = rank,
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return submissionList;
        }

        //pulling competitionSubmission from DB for editRanking
        public CompetitionSubmission GetCompetitionSubmission(int competitionid, int competitorid)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission WHERE CompetitionID = @competitionid AND CompetitorID = @competitorid";
            cmd.Parameters.AddWithValue("@competitionID", competitionid);
            cmd.Parameters.AddWithValue("@competitorID", competitorid);

            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();

            CompetitionSubmission retrievedSubmission = new CompetitionSubmission();

            //Read all records until the end, save data into a competition list

            while (reader.Read())
            {

                retrievedSubmission.CompetitionID = reader.GetInt32(0);
                retrievedSubmission.CompetitorID = reader.GetInt32(1);
                //Lazy to make viewmodel, don't need filesubmitted anyway
                retrievedSubmission.FileSubmitted = null;
                retrievedSubmission.DateTimeFileUpload = reader.GetDateTime(3);
                retrievedSubmission.Appeal = reader.GetString(4);
                retrievedSubmission.VoteCount = reader.GetInt32(5);
                retrievedSubmission.Ranking = reader.GetInt32(6);
               
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return retrievedSubmission;
        }

        public int getTotalMark(int competitorid, int competitionid)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT        CompetitionScore.CompetitorID, CompetitionScore.CompetitionID, Criteria.CriteriaID, Criteria.CompetitionID AS Expr1, Criteria.Weightage, CompetitionScore.Score, CompetitionScore.CriteriaID AS Expr2
FROM            CompetitionScore INNER JOIN
                         Criteria ON CompetitionScore.CriteriaID = Criteria.CriteriaID WHERE CompetitionScore.CompetitionID = @competitionid AND CompetitionScore.CompetitorID = @competitorid";
            cmd.Parameters.AddWithValue("@CompetitionID", competitionid);
            cmd.Parameters.AddWithValue("@CompetitorID", competitorid);

            //Open a database connection
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            int totalMark = 0;

            //Read all records until the end, save data into a competition list

            while (reader.Read())
            {

                totalMark += reader.GetInt32(4) * (reader.GetInt32(5) / 10);

            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return totalMark;
        }

        public List<Criteria> getCriteriaDetailsListByCompID(int compid)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"select * from Criteria where competitionid = @compid";
            cmd.Parameters.AddWithValue("@compid", compid);
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            List<Criteria> critList = new List<Criteria>();
            while (reader.Read())
            {
                critList.Add(
                new Criteria
                {
                    CriteriaID = reader.GetInt32(0),
                    CompetitionID = compid,
                    CriteriaName = reader.GetString(2),
                    Weightage = reader.GetInt32(3)
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return critList;

        }
    }
}
