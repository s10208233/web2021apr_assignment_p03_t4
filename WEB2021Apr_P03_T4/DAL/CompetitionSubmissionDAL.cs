﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc;

namespace WEB2021Apr_P03_T4.DAL
{
    public class CompetitionSubmissionDAL
    {
        IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public CompetitionSubmissionDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CJP_DB_ConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public bool UpdateRanking(int ranking, int competitionid, int competitorid)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE CompetitionSubmission SET Ranking=@ranking
                              WHERE CompetitorID = @competitorid AND CompetitionID = @competitionid";

            cmd.Parameters.AddWithValue("@ranking", ranking);
            cmd.Parameters.AddWithValue("@competitionid", competitionid);
            cmd.Parameters.AddWithValue("@competitorid", competitorid);

            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            return true;
        }
    }
}
