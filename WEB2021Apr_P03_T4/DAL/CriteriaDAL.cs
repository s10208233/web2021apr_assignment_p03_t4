﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using WEB2021Apr_P03_T4.Models;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace WEB2021Apr_P03_T4.DAL
{
    public class CriteriaDAL
    {
        IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public CriteriaDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CJP_DB_ConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String re  ad.
            conn = new SqlConnection(strConn);
        }

        public bool Add(Criteria criteria)
        {
            bool isSuccessful = false;
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO Criteria ( CompetitionID, CriteriaName, Weightage)
                                OUTPUT INSERTED.CriteriaID
                                VALUES(@competitionid, @criterianame, @weightage)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@competitionid", criteria.CompetitionID);
            cmd.Parameters.AddWithValue("@criterianame", criteria.CriteriaName);
            cmd.Parameters.AddWithValue("@weightage", criteria.Weightage);

            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //JudgeID after executing the INSERT SQL statement
            criteria.CriteriaID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return isSuccessful;
        }

        public int DeleteCritirion(int compid, int critirionID)
        {
            DeleteCompetitionScoreRowsBasedOnCompIDCriteriaID(compid, critirionID);
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM Criteria WHERE CompetitionID = @compid AND CriteriaID = @critirionID";
            cmd.Parameters.AddWithValue("@compid", compid);
            cmd.Parameters.AddWithValue("@critirionID", critirionID);
            conn.Open();
            int rowAffected = 0;
            rowAffected += cmd.ExecuteNonQuery();
            conn.Close();
            return rowAffected;
        }

        public int DeleteCompetitionScoreRowsBasedOnCompIDCriteriaID(int compid, int critirionID)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE from CompetitionScore WHERE CompetitionID = @compid AND CriteriaID = @critirionID";
            cmd.Parameters.AddWithValue("@compid", compid);
            cmd.Parameters.AddWithValue("@critirionID", critirionID);
            conn.Open();
            int rowAffected = 0;
            rowAffected += cmd.ExecuteNonQuery();
            conn.Close();

            return rowAffected;
        }

        public List<Criteria> retrieveCriteria(int competitionID)
        {
            List<Criteria> criteriaList = new List<Criteria>();
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Criteria WHERE CompetitionID = @competitionid";
            cmd.Parameters.AddWithValue("@competitionid", competitionID);
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            while (reader.Read())
            {
                //addinig all the criteria related to the specific competitionid and adding it into the list
                criteriaList.Add(
                new Criteria
                {
                    CriteriaID = reader.GetInt32(0),
                    CompetitionID = reader.GetInt32(1),
                    CriteriaName = reader.GetString(2),
                    Weightage = reader.GetInt32(3),
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return criteriaList;
        }
    }
}
