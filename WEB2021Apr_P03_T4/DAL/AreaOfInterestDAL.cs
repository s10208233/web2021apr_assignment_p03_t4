﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.Models;

namespace WEB2021Apr_P03_T4.DAL
{
    public class AreaOfInterestDAL
    {
        IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public AreaOfInterestDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CJP_DB_ConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        // Get List of Area Of Interest
        public List<AreaInterestViewModel> GetAreaInterestVMList()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM AreaInterest LEFT JOIN (SELECT Competition.AreaInterestID,COUNT(Competition.AreaInterestID)
                                AS CompCount FROM Competition GROUP BY Competition.AreaInterestID)s ON 
                                AreaInterest.AreaInterestID = s.AreaInterestID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<AreaInterestViewModel> AreaOfInterestVMList = new List<AreaInterestViewModel>();
            while (reader.Read())
            {
                int compCount = 0;
                try
                {
                    compCount = reader.GetInt32(3);
                }
                catch { 
                }
                AreaOfInterestVMList.Add(
                new AreaInterestViewModel
                {
                    AreaInterestID = reader.GetInt32(0),
                    Name = reader.GetString(1),
                    CompetitionCount = compCount
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return AreaOfInterestVMList;
        }

        public bool Add(AreaInterest aoi)
        {
            bool isSuccessful = false;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"INSERT INTO AreaInterest (Name)
                                OUTPUT INSERTED.AreaInterestID
                                VALUES(@name)";

            cmd.Parameters.AddWithValue("@name", aoi.Name);

            conn.Open();
            aoi.AreaInterestID = (int)cmd.ExecuteScalar();
            conn.Close();
            return isSuccessful;
        }

        public AreaInterest GetDetails(int aoiID)
        {
            AreaInterest aoi = new AreaInterest();

            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM AreaInterest WHERE AreaInterestID = @aoiID";
            cmd.Parameters.AddWithValue("@aoiID", aoiID);

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    aoi.AreaInterestID = aoiID;
                    aoi.Name = reader.GetString(1);
                }
            }
            reader.Close();
            conn.Close();
            return aoi;
        }


        public int Delete(int aoiID)
        {
            //Instantiate a SqlCommand object, supply it with a DELETE SQL statement
            //to delete a staff record specified by a Staff ID
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"DELETE FROM AreaInterest WHERE AreaInterestID = @aoiID";
            cmd.Parameters.AddWithValue("@aoiID", aoiID);
            //Open a database connection
            conn.Open();
            int rowAffected = 0;
            //Execute the DELETE SQL to remove the staff record
            rowAffected += cmd.ExecuteNonQuery();
            //Close database connection
            conn.Close();
            //Return number of row of staff record updated or deleted
            return rowAffected;
        }

    }
}
