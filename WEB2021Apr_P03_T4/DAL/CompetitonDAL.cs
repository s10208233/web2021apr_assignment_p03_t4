﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.Models;

namespace WEB2021Apr_P03_T4.DAL
{

    public class CompetitionDAL
    {
        IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public CompetitionDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
                string strConn = Configuration.GetConnectionString(
                "CJP_DB_ConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }
        //Get all competitions to show on JoinCompetition.cshtml
        public List<Competition> GetCompetition()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Competition";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a competition list
            List<Competition> competitionList = new List<Competition>();
            while (reader.Read())
            {
                DateTime? sd = null;
                DateTime? ed = null;
                DateTime? rrd = null;

                if (!reader.IsDBNull(3))
                    sd = reader.GetDateTime(3);

                if (!reader.IsDBNull(4))
                    ed = reader.GetDateTime(4);

                if (!reader.IsDBNull(5))
                    rrd = reader.GetDateTime(5);

                competitionList.Add(
                new Competition
                {
                    CompetitionID = reader.GetInt32(0),
                    AreaInterestID = reader.GetInt32(1),
                    CompetitionName = reader.GetString(2),
                    StartDate = sd,
                    EndDate = ed,
                    ResultReleasedDate = rrd
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return competitionList;
        }
        //For controller to check if user has joined a competition already or not
        public List<CompetitionSubmission> GetSubmission()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a list
            List<CompetitionSubmission> SubmissionList = new List<CompetitionSubmission>();
            while (reader.Read())
            {
                string? appeal = null;
                if (!reader.IsDBNull(4))
                {
                    appeal = reader.GetString(4);
                }
                SubmissionList.Add(
                new CompetitionSubmission
                {
                    CompetitionID = reader.GetInt32(0),
                    CompetitorID = reader.GetInt32(1),
                    Appeal = appeal
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return SubmissionList;
        }
        //Add competitior to database when user creates an account as competitior
        public int Add(Competitor competitor)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated CompetitorID after insertion
            cmd.CommandText = @"INSERT INTO Competitor (CompetitorName, Salutation, EmailAddr,
                                Password)
                                OUTPUT INSERTED.CompetitorID
                                VALUES(@name, @salu, @email,
                                @password)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            //cmd.Parameters.AddWithValue("@id", competitor.CompetitorID);
            cmd.Parameters.AddWithValue("@name", competitor.CompetitorName);
            cmd.Parameters.AddWithValue("@salu", competitor.Salutation);
            cmd.Parameters.AddWithValue("@email", competitor.EmailAddr);
            cmd.Parameters.AddWithValue("@password", competitor.Password);
            
            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //CompetitorID after executing the INSERT SQL statement
            competitor.CompetitorID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return competitor.CompetitorID;
        }
        //When competitor joins a competition insert into database
        public int JoinCompetition(CompetitionSubmission competitionSubmit)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated CompetitorID after insertion
            cmd.CommandText = @"INSERT INTO CompetitionSubmission (VoteCount, CompetitionID, CompetitorID)
                                VALUES(@voteCount, @competitionID, @competitorID)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            cmd.Parameters.AddWithValue("@voteCount", competitionSubmit.VoteCount);
            cmd.Parameters.AddWithValue("@competitionID", competitionSubmit.CompetitionID);
            cmd.Parameters.AddWithValue("@competitorID", competitionSubmit.CompetitorID);
            



            //A connection to database must be opened before any operations made.
            conn.Open();
            cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return competitionSubmit.CompetitionID;
        }
        //To upload PDF record into database when competitor submit works
        public int UploadPDF(string value,int CompetitionID,int CompetitorID)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
 
            cmd.CommandText = @"UPDATE CompetitionSubmission SET FileSubmitted" +" = '"+ value+"',"+"DateTimeFileUpload = '"+ String.Format("{0:dd-MMM-yyyy H:mm:ss}", DateTime.Now) + "' WHERE CompetitionID" + " = "+CompetitionID + " AND CompetitorID = "+CompetitorID;

            //A connection to database must be opened before any operations made.
            conn.Open();
            cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            
            return 0;
        }
        //When competitor submit appeal it is added into database
        public bool SubmitAppeal(string value, int CompetitionID, int CompetitorID)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            
            cmd.CommandText = @"UPDATE CompetitionSubmission SET Appeal" + " = '" + value + "'"  + " WHERE CompetitionID" + " = " + CompetitionID + " AND CompetitorID = " + CompetitorID;

            //A connection to database must be opened before any operations made.
            conn.Open();
            cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            if (value != null && value != " ")
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        //To get all the citeria for ViewAndAppeal controller action
        public List<Criteria> GetCriteria()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Criteria";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a list
            List<Criteria> CriteriaList = new List<Criteria>();
            while (reader.Read())
            {
                CriteriaList.Add(
                new Criteria
                {
                    CompetitionID = reader.GetInt32(1),
                    CriteriaName = reader.GetString(2)
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return CriteriaList;
        }
        //To get all competition score for ViewAndAppeal controller
        public List<CompetitionScore> GetCompetitionScore()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM CompetitionScore";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a list
            List<CompetitionScore> CompetitionScoreList = new List<CompetitionScore>();
            while (reader.Read())
            {
                CompetitionScoreList.Add(
                new CompetitionScore
                {
                    CompetitorID = reader.GetInt32(1),
                    CompetitionID = reader.GetInt32(2),
                    Score = reader.GetInt32(3)
                    
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return CompetitionScoreList;
        }
        //Email validation for create competitor
        public bool IsEmailExist(string email, int staffId)
        {
            bool emailFound = false;
            //Create a SqlCommand object and specify the SQL statement
            //to get a competitor record with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT CompetitorID FROM Competitor
                                WHERE EmailAddr=@selectedEmail";
            cmd.Parameters.AddWithValue("@selectedEmail", email);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    if (reader.GetInt32(0) != staffId)
                        //The email address is used by another competitor 
                        emailFound = true;
                }
            }
            else
            { //No record
                emailFound = false; // The email address given does not exist
            }
            reader.Close();
            conn.Close();

            return emailFound;
        }
        public List<Comment> GetComments()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM Comment";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<Comment> commentsList = new List<Comment>();
            while (reader.Read())
            {
                commentsList.Add(
                new Comment
                {
                    CommentID = reader.GetInt32(0),
                    CompetitionID = reader.GetInt32(1),
                    Description = reader.GetString(2),
                    DateTimePosted = reader.GetDateTime(3),
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return commentsList;
        }
        public List<CompetitionScore> GetScores()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM CompetitionScore";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<CompetitionScore> scoresList = new List<CompetitionScore>();
            while (reader.Read())
            {
                scoresList.Add(
                new CompetitionScore
                {
                    CriteriaID = reader.GetInt32(0),
                    CompetitorID = reader.GetInt32(1),
                    CompetitionID = reader.GetInt32(2),
                    Score = reader.GetInt32(3),
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return scoresList;
        }
        public List<PublicViewModel> GetSubmissions()
        {   
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"select cs.CompetitionID,cs.CompetitorID,Com.CompetitionName,c.CompetitorName,cs.FileSubmitted,cs.DateTimeFileUpload,cs.Appeal,
                                cs.VoteCount,cs.Ranking FROM CompetitionSubmission cs INNER JOIN Competitor c on  cs.CompetitorID= 
                                c.CompetitorID INNER JOIN Competition com ON cs.CompetitionID= com.CompetitionID";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<PublicViewModel> submissionList = new List<PublicViewModel>();
            while (reader.Read())
            {
                string fileName = "No file";
                DateTime? dtfu = null;
                string appeal = "No appeal";
                int vc = 0;
                int rank = 0;

                if (!reader.IsDBNull(4))
                    fileName = reader.GetString(4);

                if (!reader.IsDBNull(5))
                    dtfu = reader.GetDateTime(5);

                if (!reader.IsDBNull(6))
                    appeal = reader.GetString(6);

                if (!reader.IsDBNull(7))
                    vc = reader.GetInt32(7);

                if (!reader.IsDBNull(8))
                    rank = reader.GetInt32(8);

                submissionList.Add(
                new PublicViewModel
                {
                    CompetitionID = reader.GetInt32(0),
                    CompetitorID = reader.GetInt32(1),
                    CompetitionName = reader.GetString(2),
                    CompetitorName = reader.GetString(3),
                    FileSubmitted=  fileName,
                    DateTimeFileUpload = dtfu,
                    Appeal= appeal,
                    VoteCount = vc,
                    Ranking = rank,
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return submissionList;
        }
        public List<PublicViewModel> GetTopRanking()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"select cs.CompetitionID,cs.CompetitorID,Com.CompetitionName,c.CompetitorName,cs.FileSubmitted,cs.DateTimeFileUpload,cs.Appeal,
                                cs.VoteCount,cs.Ranking FROM CompetitionSubmission cs INNER JOIN Competitor c on  cs.CompetitorID= 
                                c.CompetitorID INNER JOIN Competition com ON cs.CompetitionID= com.CompetitionID  where cs.Ranking is not null order by cs.Ranking asc";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<PublicViewModel> submissionList = new List<PublicViewModel>();
            while (reader.Read())
            {
                string fileName = "No file";
                DateTime? dtfu = null;
                string appeal = "No appeal";
                int vc = 0;
                int rank = 0;

                if (!reader.IsDBNull(4))
                    fileName = reader.GetString(4);

                if (!reader.IsDBNull(5))
                    dtfu = reader.GetDateTime(5);

                if (!reader.IsDBNull(6))
                    appeal = reader.GetString(6);

                if (!reader.IsDBNull(7))
                    vc = reader.GetInt32(7);

                if (!reader.IsDBNull(8))
                    rank = reader.GetInt32(8);

                submissionList.Add(
                new PublicViewModel
                {
                    CompetitionID = reader.GetInt32(0),
                    CompetitorID = reader.GetInt32(1),
                    CompetitionName = reader.GetString(2),
                    CompetitorName = reader.GetString(3),
                    FileSubmitted = fileName,
                    DateTimeFileUpload = dtfu,
                    Appeal = appeal,
                    VoteCount = vc,
                    Ranking = rank,
                }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return submissionList;
        }
        //Package3 ozzy competition judging
        public bool UpdateRanking(int ranking, int competitionid, int competitorid)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE CompetitionSubmission SET Ranking=@ranking
                              WHERE CompetitorID = @competitorid AND CompetitionID = @competitionid";

            cmd.Parameters.AddWithValue("@ranking", ranking);
            cmd.Parameters.AddWithValue("@competitionid", competitionid);
            cmd.Parameters.AddWithValue("@competitorid", competitorid);

            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            return true;
        }
        //Package ozzy competition juding
        public bool UpdateScores(int criteriaID, int competitorID, int competitionID, int score)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE CompetitionScore SET Criteria=@criteriaid,
                              CompetitorID=@competitorid, BranchNo = @competitionid, Score=@score
                              WHERE CompetitorID = @competitorid AND CompetitionID = @competitionid AND CriteriaID = @criteriaid";

            cmd.Parameters.AddWithValue("@criteriaid", criteriaID);
            cmd.Parameters.AddWithValue("@competitionid", competitionID);
            cmd.Parameters.AddWithValue("@score", score);
            cmd.Parameters.AddWithValue("@competitorid", competitorID);

            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            return true;
        }
        //Get the datetime that results were released
        //To be used in controller/dal to stop judge from changing score
        public DateTime? GetReleasedDate(int competitionID)
        {
            DateTime? releasedDate = null;
            //Create a SqlCommand object and specify the SQL statement
            //to get a staff record with the email address to be validated
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT ResultReleasedDate FROM Competition
                                WHERE CompetitionID=@competitionid";
            cmd.Parameters.AddWithValue("@coompetitionid", competitionID);
            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            { //Records found
                while (reader.Read())
                {
                    releasedDate = reader.GetDateTime(0);
                }
            }
            reader.Close();
            conn.Close();

            return releasedDate;
        }
        public List<PublicViewModel> GetPublicViewModel()
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify the SELECT SQL statement
            cmd.CommandText = @"SELECT * FROM CompetitionSubmission";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<PublicViewModel> ViewModelList = new List<PublicViewModel>();
            while (reader.Read())
            {
                ViewModelList.Add
                (
                    new PublicViewModel
                    {
                        
                    }
                );
            }
            //Close DataReader
            reader.Close();
            //Close the database connection
            conn.Close();
            return ViewModelList;
        }
        public int AddComment(Comment comment)
        {
            //Create a SqlCommand object from connection object
            SqlCommand cmd = conn.CreateCommand();
            //Specify an INSERT SQL statement which will
            //return the auto-generated StaffID after insertion
            cmd.CommandText = @"INSERT INTO Comment (CompetitionID,Description,DateTimePosted)
                                OUTPUT INSERTED.CommentID
                                VALUES(@CompetID,@Desc,@DTP)";
            //Define the parameters used in SQL statement, value for each parameter
            //is retrieved from respective class's property.
            //cmd.Parameters.AddWithValue("@id", competitor.CompetitorID);
            cmd.Parameters.AddWithValue("@CompetID", comment.CompetitionID);
            cmd.Parameters.AddWithValue("@Desc", comment.Description);
            cmd.Parameters.AddWithValue("@DTP", DateTime.Now);

            //A connection to database must be opened before any operations made.
            conn.Open();
            //ExecuteScalar is used to retrieve the auto-generated
            //StaffID after executing the INSERT SQL statement
            comment.CommentID = (int)cmd.ExecuteScalar();
            //A connection should be closed after operations.
            conn.Close();
            //Return id when no error occurs.
            return comment.CommentID;
        }
        public void UpdateVoteCount(int compid, int compet_id, int votecount)
        {
            votecount += 1;
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"UPDATE CompetitionSubmission SET VoteCount=@vc
                              WHERE CompetitorID = @compid AND CompetitionID = @compet_id";

            cmd.Parameters.AddWithValue("@compid", compid);
            cmd.Parameters.AddWithValue("@compet_id", compet_id);
            cmd.Parameters.AddWithValue("@vc", votecount);

            //Open a database connection and execute the SQL statement
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();
        }
    }
}
