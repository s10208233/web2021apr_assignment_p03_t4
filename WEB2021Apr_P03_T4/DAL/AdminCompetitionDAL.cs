﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using WEB2021Apr_P03_T4.Models;
namespace WEB2021Apr_P03_T4.DAL
{
    public class AdminCompetitionDAL
    {

        IConfiguration Configuration { get; }
        private SqlConnection conn;
        //Constructor
        public AdminCompetitionDAL()
        {
            //Read ConnectionString from appsettings.json file
            var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json");
            Configuration = builder.Build();
            string strConn = Configuration.GetConnectionString(
            "CJP_DB_ConnectionString");
            //Instantiate a SqlConnection object with the
            //Connection String read.
            conn = new SqlConnection(strConn);
        }

        public int Add(Competition competition)
        {
            bool isSuccessful = false;
            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"INSERT INTO Competition (AreaInterestID, CompetitionName, StartDate,
                                EndDate, ResultReleasedDate)
                                OUTPUT INSERTED.CompetitionID
                                VALUES(@AreaInterestID, @Name, @StartDate, @EndDate, @ResultReleasedDate)";

            cmd.Parameters.AddWithValue("@AreaInterestID", competition.AreaInterestID);
            cmd.Parameters.AddWithValue("@Name", competition.CompetitionName);
            if (competition.StartDate == null)
            {
                cmd.Parameters.AddWithValue("@StartDate", DBNull.Value);
            }
            else {
                cmd.Parameters.AddWithValue("@StartDate", competition.StartDate);
            }
            if (competition.EndDate == null)
            {
                cmd.Parameters.AddWithValue("@EndDate", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@EndDate", competition.EndDate);
            }
            if (competition.ResultReleasedDate == null)
            {
                cmd.Parameters.AddWithValue("@ResultReleasedDate", DBNull.Value);
            }
            else
            {
                cmd.Parameters.AddWithValue("@ResultReleasedDate", competition.ResultReleasedDate);
            }

            conn.Open();
            competition.CompetitionID = (int)cmd.ExecuteScalar();
            conn.Close();
            return competition.CompetitionID;
        }

        public Competition GetDetails(int compID)
        {

            Competition comp = new Competition();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT * FROM Competition WHERE CompetitionID = @compID";
            cmd.Parameters.AddWithValue("@compID", compID);

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    DateTime? sd = null;
                    DateTime? ed = null;
                    DateTime? rrd = null;

                    if (!reader.IsDBNull(3))
                        sd = reader.GetDateTime(3);

                    if (!reader.IsDBNull(4))
                        ed = reader.GetDateTime(4);

                    if (!reader.IsDBNull(5))
                        rrd = reader.GetDateTime(5);

                    comp.CompetitionID = compID;
                    comp.AreaInterestID = reader.GetInt32(1);
                    comp.CompetitionName = reader.GetString(2);
                    comp.StartDate = sd;
                    comp.EndDate = ed;
                    comp.ResultReleasedDate = rrd;
                }
            }
            reader.Close();
            conn.Close();
            return comp;
        }

        public int Delete(int compID)
        {
            SqlCommand cmd = conn.CreateCommand();
            SqlCommand cmd2 = conn.CreateCommand();
            try
            {
                cmd.CommandText = @"DELETE FROM CompetitionJudge WHERE CompetitionID=@compID";
                cmd.Parameters.AddWithValue("@compID", compID);
                conn.Open();
                int rowAffected1 = 0;
                rowAffected1 += cmd.ExecuteNonQuery();
                conn.Close();
                cmd2.CommandText = @"DELETE FROM Competition WHERE CompetitionID = @compID";
                cmd2.Parameters.AddWithValue("@compID", compID);
                conn.Open();
                int rowAffected = 0;
                rowAffected += cmd2.ExecuteNonQuery();
                conn.Close();
                return rowAffected;
            }
            catch {
                cmd.CommandText = @"DELETE FROM Competition WHERE CompetitionID = @compID";
                cmd.Parameters.AddWithValue("@compID", compID);
                conn.Open();
                int rowAffected = 0;
                rowAffected += cmd.ExecuteNonQuery();
                conn.Close();
                return rowAffected;
            }
            
        }


        //  This my magic function to produce a list to either display 'Unassigned Judges'
        //  OR Judges that are already in a competition

        public List<CompetitionJudge_mini_ViewModel> GetUnassignedJudgesORExistingJudges12(int option)
        {
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = @"
                             SELECT        Judge.JudgeID, Judge.JudgeName, CompetitionJudge.CompetitionID, Competition.CompetitionName
                                FROM            Competition FULL OUTER JOIN
                         CompetitionJudge ON Competition.CompetitionID = CompetitionJudge.CompetitionID FULL OUTER JOIN
                         Judge ON CompetitionJudge.JudgeID = Judge.JudgeID
                                ";
            //Open a database connection
            conn.Open();
            //Execute the SELECT SQL through a DataReader
            SqlDataReader reader = cmd.ExecuteReader();
            //Read all records until the end, save data into a staff list
            List<CompetitionJudge_mini_ViewModel> theList = new List<CompetitionJudge_mini_ViewModel>();
            while (reader.Read())
            {
                int judgeID = 0;
                string judgeName = "-";
                int CompetitionID = 0;
                string CompetitionName = "-";

                //DateTime? sd = null;
                //DateTime? ed = null;
                //DateTime? rrd = null;

                if (!reader.IsDBNull(0))
                    judgeID = reader.GetInt32(0);

                if (!reader.IsDBNull(1))
                    judgeName = reader.GetString(1);

                if (!reader.IsDBNull(2))
                    judgeID = reader.GetInt32(0);

                if (!reader.IsDBNull(3))
                    judgeName = reader.GetString(1);

                theList.Add(
                new CompetitionJudge_mini_ViewModel
                {
                    JudgeID = judgeID,
                    JudgeName = judgeName,
                    CompetitionName = CompetitionName,
                    CompetitionID = CompetitionID
                }
                );
            }
            reader.Close();
            conn.Close();

            //  1 for unassigned judges with no competitions
            if (option == 1) {
                List<CompetitionJudge_mini_ViewModel> judgeWithNoComps = new List<CompetitionJudge_mini_ViewModel>();
                for (int i = 0; i < theList.Count; i++) {
                    if (theList[i].JudgeID == 0 && theList[i].CompetitionID == 0) {
                        judgeWithNoComps.Add(new CompetitionJudge_mini_ViewModel
                        {
                            JudgeID = theList[i].JudgeID,
                            JudgeName = theList[i].JudgeName,
                            CompetitionID = theList[i].CompetitionID,
                            CompetitionName = theList[i].CompetitionName
                        });
                    }
                }
                return judgeWithNoComps;
            }


            //  1 for unassigned judges with no competitions
            if (option == 2) {
                List<CompetitionJudge_mini_ViewModel> judgesWithComps = new List<CompetitionJudge_mini_ViewModel>();
                for (int i = 0; i < theList.Count; i++)
                {
                    if (theList[i].JudgeID != 0 && theList[i].CompetitionID != 0)
                    {
                        judgesWithComps.Add(new CompetitionJudge_mini_ViewModel
                        {
                            JudgeID = theList[i].JudgeID,
                            JudgeName = theList[i].JudgeName,
                            CompetitionID = theList[i].CompetitionID,
                            CompetitionName = theList[i].CompetitionName
                        });
                    }
                }
                return judgesWithComps;
            }

            return theList;
        }

    }

}
